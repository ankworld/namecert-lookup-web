import { Injectable } from '@nestjs/common';
import lookup from 'whois-json';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  async getWhoisData(): Promise<any> {
    return await lookup('e-office.cloud');
  }
}
